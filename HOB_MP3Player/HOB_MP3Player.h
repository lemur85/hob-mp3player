/**
 * @file HOB_MP3Player.h
 * @author Eloy Soto [lemur85] <el.lemur.85@gmail.com>
 * @date 22/12/2015
 * @brief File containing HOB_MP3Player class definition.
 */

#ifndef __HOB_MP3PLAYER_H__
#define __HOB_MP3PLAYER_H__

#include "Arduino.h"
#include <SoftwareSerial.h>

/**
 * \defgroup Commands Chip commands and constants values
 * @{
 */
#define CMD_NEXT_SONG 			0X01
#define CMD_PREV_SONG 			0X02
#define CMD_PLAY_W_INDEX 		0X03
#define CMD_VOLUME_UP 			0X04
#define CMD_VOLUME_DOWN 		0X05
#define CMD_SET_VOLUME 			0X06
#define CMD_SINGLE_CYCLE_PLAY 	0X08
#define CMD_SEL_DEV 			0X09
#define DEV_TF 					0X02
#define CMD_SLEEP_MODE 			0X0A
#define CMD_WAKE_UP 			0X0B
#define CMD_RESET 				0X0C
#define CMD_PLAY 				0X0D
#define CMD_PAUSE 				0X0E
#define CMD_PLAY_FOLDER_FILE 	0X0F
#define CMD_STOP_PLAY 			0X16
#define CMD_FOLDER_CYCLE 		0X17
#define CMD_SHUFFLE_PLAY 		0X18
#define CMD_SET_SINGLE_CYCLE 	0X19
#define SINGLE_CYCLE_ON 		0X00
#define SINGLE_CYCLE_OFF 		0X01
#define CMD_SET_DAC 			0X1A
#define DAC_ON  				0X00
#define DAC_OFF 				0X01
#define CMD_PLAY_W_VOL 			0X22
#define MIN_VOL		0
#define MID_VOL		15
#define MAX_VOL		30

/**@}*/

/**
 * @brief Enumerated for internal player states.
 */
typedef enum PLAYER_STATES {
	IDLE			= 0x00,
	INITIALIZING 	= 0x01,
	INITIALIZED		= 0x02
}PLAYER_STATES;

class HOB_MP3Player {
public:
	HOB_MP3Player(uint16_t NbOfSongs);
	int8_t Init(SoftwareSerial *s);
	int8_t SendCommand(uint8_t command, uint16_t data);
	int8_t PlayNextSong(void);
	int8_t PlayPreviousSong(void);
	int8_t SetVolume(uint16_t vol);
	int8_t Pause(void);
	int8_t Resume(void);
	int8_t Stop(void);

private:
	PLAYER_STATES _State = IDLE;
	uint16_t _NbOfSongs = 0;
	uint8_t _Buffer[8] = {0x00};
	SoftwareSerial *_MP3Serial;
};



#endif /* __HOB_MP3PLAYER_H__ */
