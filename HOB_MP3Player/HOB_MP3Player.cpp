/**
 * @file HOB_MP3Player.cpp
 * @author Eloy Soto [lemur85] <el.lemur.85@gmail.com>
 * @date 22/12/2015
 * @brief File containing HOB_MP3Player class implementation.
 *
 * This class implements the House Of Bots Serial MP3 Player controller logic.
 *
 * @warning This class depends on SoftwareSerial library
 */

#include "HOB_MP3Player.h"

/**
 * @brief Class constructor
 * Class constructor, takes the number of songs in SD card as input argument
 * and initializes the object.
 *
 * @param[in] NbOfSongs Number of songs in SD card.
 */
HOB_MP3Player::HOB_MP3Player(uint16_t NbOfSongs) {
	_State = IDLE;
	_NbOfSongs = NbOfSongs;
}

/**
 * @brief Initialization Function
 * This function saves a copy of the SoftwareSerial address passed as argument to
 * use it later on MP3 board communication. It also initializes the MP3 player
 * setting the SD card as device where music is stored, and sets the volume to
 * the maximum level.
 *
 * @param[in] *s Pointer to an SoftwareSerial object initialized.
 *
 * @returns 0 on success, -1 otherwise.
 */
int8_t HOB_MP3Player::Init(SoftwareSerial *s) {
	uint8_t ret = 0x00;

	_State = INITIALIZING;
	_MP3Serial = s;
	delay(100);
	ret = SendCommand(CMD_SEL_DEV, DEV_TF);
	delay(200);
	ret |= SendCommand(CMD_SET_VOLUME, MAX_VOL);

	if(ret == 0x00) {
		_State = INITIALIZED;
		return 0;
	} else {
		_State = IDLE;
		return -1;
	}
}

/**
 * @brief Function used to send a command to Catalex MP3 player board.
 * This function takes the input command code and data payload, filling the buffer
 * and sending the right command to the MP3 board.
 *
 * @param[in] command 	Command to be sent.
 * @param[in] data		Data to be sent as command payload.
 *
 * @returns 0 on success, -1 otherwise.
 */
int8_t HOB_MP3Player::SendCommand(uint8_t command, uint16_t data) {
	if(_State == IDLE) {
		return -1;
	}

	delay(20);
	_Buffer[0] = 0x7e;					//starting byte
	_Buffer[1] = 0xff;					//version
	_Buffer[2] = 0x06;					//the number of bytes of the command without starting and ending byte
	_Buffer[3] = command;				//Command
	_Buffer[4] = 0x00;					//0x00 = no feedback, 0x01 = feedback
	_Buffer[5] = (uint8_t)(data >> 8);	//Data high
	_Buffer[6] = (uint8_t)(data);		//Data low
	_Buffer[7] = 0xef;					//ending byte

	for(uint8_t i=0; i<8; i++) {
		_MP3Serial->write(_Buffer[i]) ;
	}

	return 0;
}


/**
 * @brief Function used to wrap PLAY_NEXT_SONG command.
 *
 * @returns 0 on success, -1 otherwise.
 */
int8_t HOB_MP3Player::PlayNextSong(void) {
	return SendCommand(CMD_NEXT_SONG, 0x0000);
}

/**
 * @brief Function used to wrap PLAY_PREVIOUS_SONG command.
 *
 * @returns 0 on success, -1 otherwise.
 */
int8_t HOB_MP3Player::PlayPreviousSong(void) {
	return SendCommand(CMD_PREV_SONG, 0x0000);
}

/**
 * @brief Function used to wrap SET_VOLUME command.
 *
 * @returns 0 on success, -1 otherwise.
 */
int8_t HOB_MP3Player::SetVolume(uint16_t vol) {
	/* Check volume value */
	if(vol<MIN_VOL || vol>MAX_VOL) {
		return -1;
	}
	return SendCommand(CMD_SET_VOLUME, vol);
}

/**
 * @brief Function used to wrap PAUSE command.
 *
 * @returns 0 on success, -1 otherwise.
 */
int8_t HOB_MP3Player::Pause(void) {
	return SendCommand(CMD_PAUSE, 0x0000);
}

/**
 * @brief Function used to wrap RESUME command.
 *
 * @returns 0 on success, -1 otherwise.
 */
int8_t HOB_MP3Player::Resume(void) {
	return SendCommand(CMD_PLAY, 0x0000);
}

/**
 * @brief Function used to wrap STOP command.
 *
 * @returns 0 on success, -1 otherwise.
 */
int8_t HOB_MP3Player::Stop(void) {
	return SendCommand(CMD_STOP_PLAY, 0x0000);
}


