# README #

House Of Bots library for Catalex MP3 Serial Player control. Have fun!

**It depends on SoftwareSerial Arduino library**


### What is this repository for? ###

This library allow us to control Catalex MP3 Serial board [DX board link](http://www.dx.com/es/p/uart-control-serial-mp3-music-player-module-for-arduino-avr-arm-pic-blue-silver-342439#.VnlEVCAvfiE).

Implemented functionality:

* SendCommand: To send raw commands to the board.
* PlayNextSong: Play next song.
* PlayPreviousSong: Play previous song.
* Stop: stops the player.
* Pause: Pauses reproduction.
* Resume: continues playback from pause.
* SetVolume: To set output aoduo volume. 

## Install ##

To use this library you must download the repository (or clone it), and copy the "HOB_MP3Player" directory to your arduino sketchbook library path (for example, on "/home/*USER*/sketchbook/libraries". 

It's all, you should now see it on "Sketch -> Import Library" menu on Arduino IDE.

For more information visit our blog [House of Bots](https://houseofbots.wordpress.com/)


## Usage example ##

Format an McroSD card on FAT16/32. Download example [AUDIO FILES](https://bitbucket.org/lemur85/hob-mp3player/downloads/SD_AUDIO.zip) and copy then on formatted SD root. This example plays each song one time:


```
#!Arduino
/**
 * House Of Bots MP3Player example.
 *
 * Connections:
 *  - MP3 Board GND to Arduino GND
 *  - MP3 Board VCC to Arduino 3.3v
 *  - MP3 Board RX to Arduino Pin 2 
 *  - MP3 Board TX to Arduino Pin 3
 *
 * Visit https://houseofbots.wordpress.com
 *
 */

#include <HOB_MP3Player.h>
#include <SoftwareSerial.h>

/* SoftwareSerial Object */
SoftwareSerial sw_ser(3,2);

/* HOB MP3Player object */
HOB_MP3Player player(4);

void setup(void) {
  /* Initialize SoftwareSerial and player */
  sw_ser.begin(9600);
  player.Init(&sw_ser);
}


void loop(void) {
  /* Play each song one time */
  player.PlayNextSong();
  delay(6000);
  player.PlayNextSong();
  delay(6000);
  player.PlayNextSong();
  delay(6000);
  player.PlayNextSong();
  
  while(1){}
}
```


### Contact ###

* Repo admin: lemur85 <el.lemur.85@gmail.com>
* Other community or team contact: House Of Bots <houseofbots@gmail.com>